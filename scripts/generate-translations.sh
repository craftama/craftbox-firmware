pybabel extract ./craftbox/*.py -o ./messages.pot
pybabel update -D craftbox -d ./craftbox/locale -i messages.pot -l cs
pybabel update -D craftbox -d ./craftbox/locale -i messages.pot -l en
rm messages.pot
pybabel compile -d ./craftbox/locale -D craftbox