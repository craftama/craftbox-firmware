import os.path
from os.path import join

from craftbox.conf import config
from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import sh1106
from PIL import ImageFont


def set_display_normal(menu):
    from craftbox.menu.system import reboot
    rotated = config.get('display', {'rotate': False})['rotate']
    config.set('display', {'rotate': False})
    if rotated:
        reboot(menu)


def set_display_rotate(menu):
    from craftbox.menu.system import reboot
    rotated = config.get('display', {'rotate': False})['rotate']
    config.set('display', {'rotate': True})
    if not rotated:
        reboot(menu)


class Display:

    def __init__(self, device=None, font=None):
        self.messages = []
        self._device = device
        self._font = font

    @property
    def device(self):
        if not self._device:
            engine = config.get('display',
                                {'engine': 'i2c'}).get('engine')
            if engine == 'pygame':
                from luma.emulator.device import pygame
                self._device = pygame(
                    width=128, height=64, rotate=0, mode='RGB',
                    transform='none', scale=1)
            elif engine == 'i2c':
                serial = i2c(port=1, address=0x3C)
                self._device = sh1106(serial)
                rotate = config.get('display',
                                    {'rotate': False}).get('rotate', False)
                if rotate:
                    self._device = sh1106(serial, rotate=2)
                else:
                    self._device = sh1106(serial)
            else:
                raise Exception('Unsupported display engine')
        try:
            self._device.contrast(config.get('display').get('contrast'))
        except (TypeError, AssertionError):
            pass
        return self._device

    @property
    def font(self):
        if not self._font:
            self._font = ImageFont.truetype(
                join(os.path.dirname(__file__),
                     'fonts/Montserrat-Regular.ttf'), 10)
        return self._font

    def render(self, msg, add=False, position=(0, 0), *args, **kwargs):

        with canvas(self.device) as draw:

            if add:
                self.messages.append(msg)
                _msg = "\n".join([self.messages])
                draw.text(position, _msg, fill="white",
                          font=self.font, *args, **kwargs)
            else:
                draw.text(position, msg, fill="white",
                          font=self.font, *args, **kwargs)


display = Display()
