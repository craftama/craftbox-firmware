
from craftbox.conf import config
from craftbox.menu import Menu, MenuItem, back

switches = []


def _device(switch):
    unit = switch['attributes'].get('unit_of_measurement', None)
    return '%s (%s %s)' % (switch['attributes']['friendly_name'],
                           switch['state'].upper(),
                           unit)


def render_sensors(menu):

    menu.on_press_callback = back

    states = config.hass('api/states').json()

    for state in states:

        parts = state.get('entity_id').split('.')

        if parts[0] == 'sensor':
            switches.append(state)

    list_switches = [
        MenuItem(_device(switch))
        for switch in switches]

    submenu = Menu(items=list_switches, parent=menu)
    menu.active_submenu.append(submenu)
    menu.render()
