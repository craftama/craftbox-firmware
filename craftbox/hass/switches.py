import json
from craftbox import _
from craftbox.menu import Menu, MenuItem
from craftbox.conf import config
switches = []


def _device(switch):
    return '%s (%s)' % (switch['attributes']['friendly_name'],
                        switch['state'].upper())


def close_callback(menu):
    global switches
    menu.step = False

    active = menu._get_active_submenu().get_active()

    for switch in switches:
        if _device(switch) == active.name:

            menu.display.render(_('\n\nSwitching %s...' % active.name))

            res = config.hass(
                'api/services/switch/toggle',
                method='POST',
                data=json.dumps({'entity_id': switch['entity_id']}))
            res.raise_for_status()

    switches = []
    menu.active_submenu = []
    menu.on_press_callback = None


def render_switches(menu):

    states = config.hass('api/states').json()

    global switches

    menu.on_press_callback = close_callback

    for state in states:

        parts = state.get('entity_id').split('.')

        if parts[0] == 'switch':
            switches.append(state)

    list_switches = [
        MenuItem(_device(switch))
        for switch in switches]

    submenu = Menu(items=list_switches, parent=menu)
    menu.active_submenu.append(submenu)
    submenu.render()
