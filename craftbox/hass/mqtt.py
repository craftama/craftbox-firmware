import time

from craftbox import _

RENDER = True


def close_callback(menu):
    global RENDER
    RENDER = False
    menu.step = False
    menu.on_press_callback = None


def on_connect(client, userdata, flags, rc):
    client.menu.display.render(_("\n\n waiting for data..."),
                               position=(0, 10))
    client.subscribe('craftbox')


def on_message(client, userdata, msg):
    client.menu.display.render(msg.payload.decode("utf-8"),
                               position=(0, 10))


def render_hass(menu):

    import paho.mqtt.client as mqtt

    client = mqtt.Client(protocol=mqtt.MQTTv311)
    client.on_connect = on_connect
    client.on_message = on_message
    client.menu = menu

    client.connect(
        'localhost',
        port=1883,
        keepalive=60)

    global RENDER

    menu.on_press_callback = close_callback

    client.loop_start()

    while RENDER:
        time.sleep(10)

    client.loop_stop(force=True)
    RENDER = True
