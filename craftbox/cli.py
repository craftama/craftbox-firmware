#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import os.path
import time
from multiprocessing import Process

import click
from craftbox import BANNER, _
from craftbox.btconf import main as btconf
from craftbox.btconf import get_ip_address
from craftbox.menu.items.clock import render_clock
from craftbox.cloud import (
    CloudItem,
    RemoteControl,
    disable_auto_update,
    disable_remote_control,
    enable_auto_update,
    enable_remote_control,
    register_device,
    save_settings,
    update_settings
)
from craftbox.conf import config
from craftbox.display import display, set_display_normal, set_display_rotate
from craftbox.hass.mqtt import render_hass
from craftbox.hass.sensors import render_sensors
from craftbox.hass.switches import render_switches
from craftbox.menu import Menu, MenuItem, back
from craftbox.menu.system import halt, reboot, upgrade
from PIL import Image
from wifi import Cell
import pprint

pp = pprint.PrettyPrinter(indent=4)

BT_CONF = None
click.disable_unicode_literals_warning = True


def init_sequence():
    if display.support_draw:
        img_path = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                'images', 'c-logo.png'))
        logo = Image.open(img_path).convert("RGBA")
        fff = Image.new(logo.mode, logo.size, (255,) * 4)

        background = Image.new("RGBA", display.device.size, "white")

        for angle in range(0, 360, 2):
            rot = logo.rotate(angle, resample=Image.BILINEAR)
            rot = rot.resize((68, 68), Image.ANTIALIAS)
            img = Image.composite(rot, fff, rot)
            posn = ((display.device.width - rot.width) // 2, 0)
            background.paste(img, posn)
            display.device.display(
                background.convert(display.device.mode))
            time.sleep(1)
            break

    display.render(BANNER, center=True)
    time.sleep(1)


def render_info(menu):
    """Render info"""
    display.render(BANNER)
    menu.on_press_callback = back


def set_language(menu):
    """Set language"""
    config.set('language', menu.step[3].lower())
    reboot(menu)


def list_wifi(menu):
    Cells = Cell.all('wlan0')

    wifi_info = []

    cells = [c for c in Cells]

    for current in range(len(cells)):
        wifi_info += [[cells[current].ssid, True]]

    submenu = Menu(wifi_info, parent=menu,
                   encoder=menu.encoder)
    menu.active_submenu.append(submenu)
    submenu.render()


def disable_bluetooth(menu):
    config.set('bluetooth', False)
    global BT_CONF
    BT_CONF.terminate()


def enable_bluetooth(menu):
    config.set('bluetooth', True)
    global BT_CONF
    BT_CONF = Process(target=btconf)
    BT_CONF.start()


def connection_dash(menu):
    global BT_CONF

    display.render(_('\n\nchecking connections..'))

    def check_internet():
        import requests
        try:
            res = requests.get('https://google.com', timeout=1)
            res.raise_for_status()
            return _("Works")
        except Exception:
            return _("Not works")

    _d = _("Bluetooth: {bluetooth}\nWIFI: {wifi}\n"
           "Address: {address}\nInternet: {internet}")

    menu.display.render(_d.format(
        bluetooth=_("Enabled") if BT_CONF else _("Disabled"),
        address=get_ip_address(),
        wifi=config.get('wifi', {}).get('ssid', ''),
        internet=check_internet()),
        position=(3, 3),
        center=True)


def render_plugins(menu):
    """Render list of active plugins"""
    menu.close()


def install_plugins(menu):
    """Render list of available plugins with installation callback"""
    menu.close()


def disable_beta(menu):
    """Generic menu close"""
    config.set('beta', False)
    menu.flush()
    menu.close()


def enable_beta(menu):
    """Generic menu close"""
    config.set('beta', True)
    menu.flush()
    menu.close()


def start_craftbox(skip_intro=False):

    # enable bluetooth config service
    if config.get('bluetooth', False):
        enable_bluetooth(None)

    try:
        if not skip_intro:
            init_sequence()
        else:
            display.render('')  # init display device
    except Exception as e:
        print('Display initialization failed with %s' % e)
        print('Fallback to mode without display')
        while True:
            time.sleep(0.5)
    else:
        menu = Menu(display, [
            MenuItem(_("Clock"), render_clock, only_draw=True),
            MenuItem(_("Dashboard"), render_hass, beta=True),
            MenuItem(_("Sensors"), render_sensors, beta=True),
            MenuItem(_("Switches"), render_switches, beta=True),
            MenuItem(_("Settings"), items=[
                MenuItem(_("Language"), items=[
                    MenuItem(_("Czech"), set_language, 'czech'),
                    MenuItem(_("English"), set_language, 'english')
                ]),
                MenuItem(_("Bluetooth"), items=[
                    MenuItem(_("Enable"), enable_bluetooth),
                    MenuItem(_("Disable"), disable_bluetooth),
                ]),
                MenuItem(_("Wifi"), list_wifi, beta=True),
                MenuItem(_("Upgrade"), items=[
                    MenuItem(_("No"), back),
                    MenuItem(_("Yes"), upgrade)
                ]),
                MenuItem(_("Display Rotation"), items=[
                    MenuItem(_("0°"), set_display_normal),
                    MenuItem(_("180°"), set_display_rotate)
                ]),
                MenuItem(_("Beta (Advanced)"), items=[
                    MenuItem(_("Disable"), disable_beta),
                    MenuItem(_("Enable"), enable_beta)
                ]),
                CloudItem(_("Save Settings"), save_settings, beta=True),
                CloudItem(_("Update Settings"), update_settings, beta=True),
                CloudItem(_("Auto Update"), items=[
                    MenuItem(_("Enable"), enable_auto_update),
                    MenuItem(_("Disable"), disable_auto_update),
                ], beta=True),
                RemoteControl(_("Remote Control"), items=[
                    MenuItem(_("Enable"), enable_remote_control),
                    MenuItem(_("Disable"), disable_remote_control),
                ], beta=True),
                MenuItem(_("Active Plugins"), render_plugins, beta=True),
                MenuItem(_("Install Plugins"), install_plugins, beta=True),
                CloudItem(_("Register Device"), register_device, beta=True),
                MenuItem(_("System Info"), render_info),
            ]),
            MenuItem(_("Connectivity"), connection_dash),
            MenuItem(_("Restart"), items=[
                MenuItem(_("No"), back),
                MenuItem(_("Yes"), reboot)
            ]),
            MenuItem(_("Halt"), items=[
                MenuItem(_("No"), back),
                MenuItem(_("Yes"), halt)
            ]),
        ])

        menu.loop()


@click.group()
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def cli(ctx, debug, **kw):
    ctx.obj['debug'] = debug


@cli.command()
@click.option('--pc/--rpi', default=False)
@click.option('--skip-intro/--rpi', default=False)
@click.pass_context
def run(ctx, pc=False, skip_intro=False):
    """Start firmware"""
    try:
        config.set('debug', ctx.obj['debug'])
        start_craftbox(skip_intro)
    except KeyboardInterrupt:
        pass
    finally:
        config.set('debug', False)


@cli.command(name="config")
@click.pass_context
def show_config(ctx):
    """Print config"""

    pp.pprint(config._conf)
    print('Location: %s' % config._path)


@cli.command()
@click.option('--module', help='Dot path to module', required=True)
@click.option('--value', help='Value to set', required=True)
@click.pass_context
def configure(ctx, module, value):
    """Configure basics"""

    try:
        root, sub = module.split('.')
        config.set(root, {sub: str(value)})
    except ValueError:
        config.set(module, str(value))

    pp.pprint(config._conf)


@cli.command(name="upgrade")
@click.pass_context
def upgrade_cli(ctx, module, value):
    """Upgrade"""
    os.system("cd /srv/craftbox-firmware;git pull;"
              "pip3 install requirements/default.txt")
    print("Upgrade successfull")


def __main__():
    cli(obj={})


if __name__ == '__main__':
    __main__()
