import base64
import hashlib
import json
import time
from uuid import getnode

from craftbox import _
from craftbox.conf import config
from craftbox.menu import MenuItem
from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport
from requests.compat import urljoin


client = Client(transport=RequestsHTTPTransport(
    url=urljoin(config.get('cloud',
                           {'api': 'http://10.0.0.10:8000'}).get('api'),
                "api/"),
    use_json=True))


class CloudItem(MenuItem):

    def allowed(self, menu):
        allowed = super(CloudItem, self).allowed(menu)
        return allowed and config.get('cloud', {})


class RemoteControl(CloudItem):

    def allowed(self, menu):
        allowed = super(CloudItem, self).allowed(menu)
        return allowed and config.get('cloud', {}).get('token') is not None


def _get_device_id():
    try:
        result = client.execute(gql('''
            mutation {
              generateCode {
                code
              }
            }
        '''))
    except Exception as e:
        print(e)
    else:
        return result['generateCode']['code']


def register_device(menu):

    device_id = config.get('device_id', None)

    if not device_id:
        device_id = _get_device_id()
        if not device_id:
            menu.display.render(_("Something went wrong"))
            menu.close()
            return
        config.set('device_id', device_id)

    menu.display.render(_(
        "\n\nCode: %s\n"
        "waiting for registration..." % ' '.join(list(device_id))))

    max_retries = 5
    retries = 0

    while retries < max_retries:

        # pool api for registration response
        time.sleep(10)
        retries += 1

        try:
            result = client.execute(gql('''
                mutation {
                  verifyDevice(deviceId: "%s") {
                    token
                    device {
                      id
                      name
                      description
                      extra
                    }
                  }
                }
            ''' % device_id))
        except Exception as e:
            print(e)
        else:
            device = result['verifyDevice']['device']
            config.set('name', device['name'])
            if 'token' in result['verifyDevice']:
                config.set('cloud', {'token': result['verifyDevice']['token']})
            elif not config.get('cloud', {}).get('token', None):
                menu.display.render(_("\n\nDevice failed\n"
                                      "device is already registered"))
                time.sleep(10)
                break
                menu.close()

            if config.get('cloud', {}).get('token', None):
                menu.display.render(_("\n\nDevice sucessfully\nregistered..."))
                time.sleep(10)
                break
                # TODO(majklk) make this optional
                # this test registration
                save_settings(menu)

    menu.display.render(_("\n\nDevice registered\n failed!"))
    menu.close()


def save_settings(menu):
    """Save device settings in remote service"""

    result = client.execute(gql('''
        mutation updateDevice($token:String $settings: JSONString) {
          updateDevice(token: $token, settings: $settings) {
            device {
              code
            }
          }
        }
    '''), {
        'token': config.get('cloud').get('token'),
        'settings': json.dumps(config._conf)
    })
    assert result['updateDevice']['device']['code'] == config.device_id

    menu.display.render(_("\n\nSettinges sucessfully\n saved."))
    time.sleep(5)
    menu.close()


def update_settings(menu):
    """Update device settings from remote service"""
    result = client.execute(gql('''
        query resolveDevice($token:String){
          device(token: $token) {
            code
            extra
          }
        }
    '''), {
        'token': config.get('cloud').get('token')
    })
    assert result['device']['code'] == config.device_id

    try:
        extra = json.loads(result['device']['extra'])
    except Exception as e:
        menu.display.render(_(e))
    else:
        settings = extra.get('settings', {})

        if settings:
            config._conf = settings
            config.save()

        menu.display.render(_("\n\nSettings sucessfully\n updated."))

    time.sleep(5)
    menu.close()


def enable_remote_control(menu):
    config.set('cloud', {'remote_control': True})
    menu.close()


def disable_remote_control(menu):
    config.set('cloud', {'remote_control': False})
    menu.close()


def enable_auto_update(menu):
    config.set('cloud', {'auto_update': True})
    menu.close()


def disable_auto_update(menu):
    config.set('cloud', {'auto_update': False})
    menu.close()
