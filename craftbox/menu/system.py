import os

from craftbox import VERSION, _


def upgrade(menu):
    print("Upgrade started")
    menu.display.render(_("\n\nVersion: %s\nupgrade....") % VERSION)
    os.system("cd /srv/craftbox-firmware;git pull;"
              "pip3 install requirements/default.txt")
    menu.display.render(_("\n\nSystem reboot..."))
    reboot(menu)


def reboot(menu):
    menu.display.render(_("\n\nReboot started..."))
    print("system reboot triggered")
    result = os.system("reboot")
    menu.display.render(result)
    print(result)


def halt(menu):
    menu.display.render(_("\n\nSystem halted."))
    print("system halt triggered")
    result = os.system("halt")
    print(result)
