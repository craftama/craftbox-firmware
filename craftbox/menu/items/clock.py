#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2014-18 Richard Hull and contributors
# See LICENSE.rst for details.
# PYTHON_ARGCOMPLETE_OK

"""
An analog clockface with date & time.

Ported from:
https://gist.github.com/TheRayTracer/dd12c498e3ecb9b8b47f#file-clock-py
"""

import datetime
import math
import time

from craftbox.conf import config
from luma.core.render import canvas

RENDER = True


def posn(angle, arm_length):
    dx = int(math.cos(math.radians(angle)) * arm_length)
    dy = int(math.sin(math.radians(angle)) * arm_length)
    return (dx, dy)


def close_clock(menu):
    global RENDER
    RENDER = False
    menu.step = False
    menu.on_press_callback = None
    menu.active_submenu = []
    menu.render()


def render_clock(menu):
    device = menu.display.device
    today_last_time = "Unknown"

    menu.on_press_callback = close_clock

    global RENDER

    while RENDER:
        # pull down loop needs check
        menu.control.check()
        now = datetime.datetime.now(config.timezone)
        today_date = now.strftime("%d %b %y")
        today_time = now.strftime("%H:%M:%S")
        if today_time != today_last_time:
            today_last_time = today_time
            with canvas(device) as draw:
                now = datetime.datetime.now(config.timezone)
                today_date = now.strftime("%d %b %y")

                margin = 4

                cx = 30
                cy = min(device.height, 64) / 2

                left = cx - cy
                right = cx + cy

                hrs_angle = 270 + (30 * (now.hour + (now.minute / 60.0)))
                hrs = posn(hrs_angle, cy - margin - 7)

                min_angle = 270 + (6 * now.minute)
                mins = posn(min_angle, cy - margin - 2)

                sec_angle = 270 + (6 * now.second)
                secs = posn(sec_angle, cy - margin - 2)

                draw.ellipse((left + margin, margin,
                              right - margin,
                              min(device.height, 64) - margin),
                             outline="white")
                draw.line((cx, cy, cx + hrs[0], cy + hrs[1]), fill="white")
                draw.line((cx, cy, cx + mins[0], cy + mins[1]), fill="white")
                draw.line((cx, cy, cx + secs[0], cy + secs[1]), fill="red")
                draw.ellipse((cx - 2, cy - 2, cx + 2, cy + 2),
                             fill="white", outline="white")
                draw.text((2 * (cx + margin), cy - 8),
                          today_date, fill="yellow",
                          font=menu.display.font)
                draw.text((2 * (cx + margin), cy + 4),
                          today_time, fill="yellow",
                          font=menu.display.font)

        time.sleep(0.1)

    RENDER = True
    return
