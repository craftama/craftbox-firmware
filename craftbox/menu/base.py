
from craftbox import _
from craftbox.conf import config
from craftbox.display import display
from craftbox.menu.control import (
    EVENT,
    QUEUE,
    GPIOController,
    KeyboardController,
    Volume
)


def back(menu):
    if menu.parent:
        menu.parent.render()
        menu.parent.active_submenu = []
        menu.active_submenu = []
        menu.on_press_callback = None
        menu.get_active_volume().volume = menu.find_item(menu.get_active())[0]
        return
    menu.get_active_volume().volume = menu.find_item(menu.get_active())[0]
    menu.on_press_callback = None
    menu.active_submenu = []
    return


class MenuItem(object):

    name = None
    code = None

    def __init__(self, name=None, handler=None,
                 code=None, beta=False, items=[], only_draw=False):
        if not self.name:
            self.name = name
        if not self.code:
            self.code = code
        self.handler = handler
        self.beta = beta
        self.items = items
        self._selected = False
        self.only_draw = only_draw

    @property
    def has_submenu(self):
        return True if self.items else False

    def on_press_handler(self, menu):
        """make action or create sublist"""
        if self.handler:
            return self.handler(menu)

    def selected(self, menu):
        """Returns boolean"""
        return self._selected

    def allowed(self, menu):
        """Returns boolean"""
        if self.beta and not config.get('beta'):
            return False
        if self.only_draw and not menu.display.support_draw:
            return False
        return True


class Menu:
    """
    items = [("Item 1", True, menu or on_press)]
    """

    def __init__(self, display=None, items=[],
                 parent=None, control=None, push=None):
        self.items = items
        self.display = parent.display if parent else display
        self.run = True
        self.step = False
        self.parent = parent
        self.active_submenu = []
        self.on_press_callback = None
        self.menus = []

        ctl_conf = config.get('control', {
            'gpio_a': 26,
            'gpio_b': 19,
            'gpio_button': 13
        })

        if control:
            self.control = control
        else:
            engine = config.get('control',
                                {'engine': 'gpio'}).get('engine')

            if engine == 'keyboard':
                self.control = KeyboardController(
                    callback=self.on_turn, button_callback=self.on_press)
            elif engine == 'gpio':
                self.control = (parent.control if parent else GPIOController(
                    ctl_conf['gpio_up'], ctl_conf['gpio_down'],
                    callback=self.on_turn,
                    buttonPin=ctl_conf['gpio_button'],
                    buttonCallback=self.on_press))
            else:
                raise Exception("Unsupported control engine")

        self.volume = Volume(total=(len(self.get_items())
                                    if self.parent
                                    else len(self.get_items()) - 1))
        self.set_active()
        # signal.signal(signal.SIGINT, self.on_exit)

    def close(self, clean=False):
        self.render_parent()
        if clean:
            self.parent.active_submenu = []
            return
        if self.active_submenu:
            i = len(self.active_submenu) - 1
            self.active_submenu.pop(i)
            return

    def _get_submenu(self, items):
        return Menu(display=self.display,
                    items=items, parent=self,
                    control=self.control)

    def flush(self):
        for submenu in self.active_submenu:
            del submenu._items
        del self._items

    def on_press(self, value=None):

        if self.step and self.on_press_callback:
            self.on_press_callback(self)
            self.step = False
            self.on_press_callback = None
            return

        if self.step:
            self.step = None
            return

        active = self._get_active_submenu().get_active()

        if active.has_submenu:
            submenu = self._get_submenu(active.items)
            self.active_submenu.append(submenu)
        elif active.handler is not None:
            self.step = active
            # call simple callback
            EVENT.set()
            return active.handler(self)

        EVENT.set()

    def on_turn(self, delta):
        QUEUE.put(delta)
        EVENT.set()

    def consume_queue(self):
        while not QUEUE.empty():
            delta = QUEUE.get()
            self._get_active_submenu().handle_delta(delta)

    def _get_active_submenu(self):
        if self.active_submenu:
            return self.active_submenu[len(self.active_submenu) - 1]
        return self

    def render_parent(self):
        self.active_submenu[len(self.active_submenu) - 2].render()

    def get_active_volume(self):
        return (self._get_active_submenu().volume
                if self.active_submenu else self.volume)

    def handle_delta(self, delta):
        volume = self.get_active_volume()
        if delta >= 1:
            volume.up()
        else:
            volume.down()
        self.set_active()
        self.render()

    def loop(self):
        self.set_active()

        while self.run:
            EVENT.wait(0.025)
            self.control.check()
            self.consume_queue()
            if not self.on_press_callback:
                self.render()
            EVENT.clear()

    def on_exit(self, a, b):
        print("Exiting...")
        self.control.destroy()

    def is_nested(self):
        return True if self.parent is not None else False

    def get_items(self):
        if not hasattr(self, '_items'):
            items = (self.items +
                     [MenuItem(_("Back"), back)]
                     if self.is_nested()
                     else self.items)
            self._items = [item for item in items if item.allowed(self)]
        return self._items

    def render_items(self):
        active_ix = self.find_item(self.get_active())[0]

        per_page = 4
        current_page = divmod(active_ix if active_ix else 1, per_page)[0] + 1
        end = current_page * per_page
        start = current_page * per_page - per_page
        items = [item for i, item in enumerate(self.get_items())
                 if i >= start and i < end]

        return "\n".join([("> %s" % item.name
                           if item.selected(self)
                           else "  %s" % item.name)
                          for item in items
                          if item.allowed(self)])

    def get_active(self):
        """Returns active item with fallback"""
        active = [item for item in self.get_items()
                  if item.selected(self) is True]
        if any(active):
            return active[0]
        return self.get_items()[0]

    def find_item(self, item):
        """Returns index and item"""
        for i, _item in enumerate(self.get_items()):
            if _item.name == item.name:
                return i, _item

    def set_active(self):
        volume = self.get_active_volume()
        for i, item in enumerate(self.get_items()):
            i, _item = self.find_item(item)
            if i == volume.volume:
                self._items[i]._selected = True
            else:
                self._items[i]._selected = False

    def render(self):
        active = self._get_active_submenu()
        display.render(active.render_items(), position=(0, 10))
