
from craftbox.conf import config
import threading

try:
    from queue import Queue
except ImportError:
    from multiprocessing import Queue

# When the knob is turned, the callback happens in a separate thread. If
# those turn callbacks fire erratically or out of order, we'll get confused
# about which direction the knob is being turned, so we'll use a queue to
# enforce FIFO. The callback will push onto a queue, and all the actual
# volume-changing will happen in the main thread.
QUEUE = Queue()

# When we put something in the queue, we'll use an event to signal to the
# main thread that there's something in there. Then the main thread will
# process the queue and reset the event. If the knob is turned very quickly,
# this event loop will fall behind, but that's OK because it consumes the
# queue completely each time through the loop, so it's guaranteed to catch up.
EVENT = threading.Event()


def debug(str):
    if not config.debug:
        return
    print(str)


class Controller(object):

    def check(self):
        pass


class GPIOController(Controller):
    """
    A class to decode mechanical rotary encoder pulses.

    Ported to RPi.GPIO from the pigpio sample here:
    http://abyz.co.uk/rpi/pigpio/examples.html
    """

    def __init__(self, gpioA, gpioB, callback=None,
                 buttonPin=None, buttonCallback=None):
        """
        Instantiate the class. Takes three arguments: the two pin numbers to
        which the rotary encoder is connected, plus a callback to run when the
        switch is turned.

        The callback receives one argument:
        a `delta` that will be either 1 or -1.
        One of them means that the dial is
        being turned to the right; the other
        means that the dial is being turned
         to the left. I'll be damned if I know
        yet which one is which.
        """

        self.lastGpio = None
        self.gpioA = gpioA
        self.gpioB = gpioB
        self.callback = callback

        self.gpioButton = buttonPin
        self.buttonCallback = buttonCallback

        self.levA = 0
        self.levB = 0

        try:
            from RPi import GPIO
        except (RuntimeError, ImportError):
            pass
        else:
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.gpioA, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.gpioB, GPIO.IN, pull_up_down=GPIO.PUD_UP)

            GPIO.add_event_detect(self.gpioA, GPIO.BOTH, self._callback)
            GPIO.add_event_detect(self.gpioB, GPIO.BOTH, self._callback)

            if self.gpioButton:
                GPIO.setup(self.gpioButton, GPIO.IN, pull_up_down=GPIO.PUD_UP)
                GPIO.add_event_detect(self.gpioButton,
                                      GPIO.FALLING,
                                      self._buttonCallback,
                                      bouncetime=250)

    def destroy(self):
        try:
            from RPi import GPIO
        except RuntimeError:
            pass
        else:
            GPIO.remove_event_detect(self.gpioA)
            GPIO.remove_event_detect(self.gpioB)
            GPIO.cleanup()

    def _buttonCallback(self, channel):
        try:
            from RPi import GPIO
        except RuntimeError:
            pass
        else:
            self.buttonCallback(GPIO.input(channel))

    def _callback(self, channel):
        try:
            from RPi import GPIO
        except RuntimeError:
            pass
        else:
            level = GPIO.input(channel)

            if channel == self.gpioA:
                self.levA = level
            else:
                self.levB = level

            # Debounce.
            if channel == self.lastGpio:
                return

            # When both inputs are at 1,
            # we'll fire a callback. If A was the most
            # recent pin set high, it'll be forward,
            # and if B was the most recent pin
            # set high, it'll be reverse.
            self.lastGpio = channel
            if channel == self.gpioA and level == 1:
                if self.levB == 1:
                    self.callback(1)
            elif channel == self.gpioB and level == 1:
                if self.levA == 1:
                    self.callback(-1)


class KeyboardController(Controller):

    def __init__(self, callback=None, button_callback=None):

        self.callback = callback
        self.button_callback = button_callback

    def check(self):
        import pygame

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    self.button_callback(event)
                if event.key == pygame.K_DOWN:
                    self.callback(1)
                if event.key == pygame.K_UP:
                    self.callback(-1)


class VolumeError(Exception):
    pass


class Volume:
    """
    A wrapper API for interacting with the volume settings on the RPi.
    """
    MIN = 0
    MAX = 100
    INCREMENT = 1

    def __init__(self, total, start=0):
        # Set an initial value for last_volume in case we're muted when we
        # start.
        self.MAX = total
        self.last_volume = self.MIN
        self.volume = start
        self.is_muted = False

        # self._sync()

    def up(self):
        """
        Increases the volume by one increment.
        """
        return self.change(self.INCREMENT)

    def down(self):
        """
        Decreases the volume by one increment.
        """
        return self.change(-self.INCREMENT)

    def change(self, delta):
        v = self.volume + delta
        v = self._constrain(v)
        return self.set_volume(v)

    def set_volume(self, v):
        """
        Sets volume to a specific value.
        """
        self.volume = self._constrain(v)
        return self.volume

    def toggle(self):
        """
        Toggles muting between on and off.
        """

        if not self.is_muted:
            self.set_volume(self.last_volume)
        return self.is_muted

    def status(self):
        if self.is_muted:
            return "{}% (muted)".format(self.volume)
        return "{}%".format(self.volume)

    # Ensures the volume value is between our minimum and maximum.
    def _constrain(self, v):
        if v < self.MIN:
            return self.MIN
        if v > self.MAX:
            return self.MAX
        return v
