import errno
import os
import os.path

import anyconfig
import appdirs
import pytz
import requests


MERGEABLES = (list, tuple, dict)


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def merge(a, b):
    """Merge tuples, lists or dictionaries without duplicates
    """
    if isinstance(a, MERGEABLES) \
            and isinstance(b, MERGEABLES):
        # dict update
        if isinstance(a, dict) and isinstance(b, dict):
            a.update(b)
            return a
        # list update
        _a = list(a)
        for x in list(b):
            if x not in _a:
                _a.append(x)
        return _a
    if a and b:
        raise Exception("Cannot merge")
    raise NotImplementedError


class CraftboxConf:

    APP_NAME = "Craftbox-Firmware"

    def __init__(self, path=None):

        if not path:
            path = os.path.join(appdirs.user_config_dir(CraftboxConf.APP_NAME),
                                'config.yml')
        self._path = path
        self.initialized = False

    @property
    def device_id(self):
        return self.get('device_id')

    def hass(self, uri, method='GET', **kwargs):

        s = requests.Session()
        hass_conf = self.get('home_assistant', {})

        if hass_conf.get('password', None):
            s.headers.update('x-ha-access', hass_conf['password'])

        url = '%s://%s:%s/%s' % (hass_conf.get('protocol', 'http'),
                                 hass_conf.get('host', 'localhost'),
                                 hass_conf.get('port', 8123),
                                 uri)

        return getattr(s, method.lower())(url, **kwargs)

    @property
    def debug(self):
        return self.get('debug', False)

    @property
    def timezone(self):
        return pytz.timezone(self.get('timezone'))

    def init(self, default_conf={'timezone': 'Europe/Prague'}):
        mkdir_p('/'.join(self._path.split("/")[:-1]))
        try:
            self._conf = anyconfig.load(self._path)
        except Exception as e:
            print("Cannot load config from path: %s, %s" % (
                self._path, e))
            self._conf = default_conf
            self.save()
        self.initialized = True

    def get(self, key, default=None):
        return self._conf.get(key, default)

    def set(self, key, data, save=True):
        try:
            self._conf[key] = merge(self._conf[key], data)
        except:
            self._conf[key] = data
        self.save() if save else None

    def save(self):
        anyconfig.dump(self._conf, self._path, 'yaml')


config = CraftboxConf()
