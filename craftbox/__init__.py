import gettext
import locale
import os.path

from craftbox.conf import config

VERSION = "0.0.2"

config.init({
    'timezone': 'Europe/Prague',
    'beta': False,
    'language': 'czech',
    'bluetooth': True,
    'control': {
        'gpio_up': 26,
        'gpio_down': 19,
        'gpio_button': 13,
        'engine': 'gpio'
    },
    'version': VERSION,
    'home_assistant': {
        'host': 'localhost',
        'port': 8123,
        'password': 'craftbox'
    },
    'display': {
        'rotate': False,
        'contrast': 155,
        'engine': 'lcd'
    },
    'cloud': {
        'api': 'https://craftbox.cloud'
    }
})

LOCALE_MAP = {
    'czech': 'cs_CZ.UTF-8',
    'english': 'en_GB.UTF-8',
}

locale.setlocale(locale.LC_ALL, LOCALE_MAP[config.get('language')])
gettext.textdomain('craftbox')
gettext.bindtextdomain('craftbox', os.path.join(os.path.dirname(__file__),
                                                'locale/'))
gettext.bind_textdomain_codeset("default", 'UTF-8')
lang = gettext.translation('craftbox',
                           os.path.join(os.path.dirname(__file__),
                                        'locale/'),
                           languages=[config.get('language')],
                           fallback=True)
_ = lang.gettext
lang.install()


BANNER = _("""
           CRAFTBOX\n
          VERSION: %s
    """) % VERSION
