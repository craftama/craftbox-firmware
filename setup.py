from setuptools import setup

setup(
    setup_requires=['pbr>=1.9', 'setuptools>=34.3.3'],
    pbr=True,
)
