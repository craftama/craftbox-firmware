
=================
Craftbox Firmware
=================

Simple control program written in python. Supports various control(GPIO, Keyboard) and display(OLED, LCD, PyGame) combinations. Integration with Home Assistant sensors and switches. MQTT Dashboard for rendering custom data.

Features
========

- OLED, LCD, PyGame display
- Simple nested menu with knob controll
- Dynamic menu items
- Craftbox Settings
- MQTT Dashboard
- Clock
- System Info
- Bluetooth Config Service
- Locales / Timezones
- Cloud integration (Save, Update settings, Remote control)

Usage
=====

.. code-block:: bash

    craftbox show_config
    craftbox run

Config
======

Craftbox uses `anyconfig` then you can use any format which you want, but you must install extra dependencies, in default yaml/json is supported.

.. code-block:: yaml

    debug: true
    control:
        engine: keyboard/gpio
        # if gpio is specified
        gpio_up: 26 # default
        gpio_down: 19 # default
        gpio_button: 13 # default

    display:
        engine: pygame/oled/lcd
        rotate: false
        contrast: 255

    home_assistant:
        host: localhost
        port: 8123
        password: password

    language: czech
    timezone: Europe/Prague

Run on rpi with oled display and potentiometer control.

.. code-block:: yaml

    control:
        engine: gpio
        gpio_up: 26
        gpio_down: 19
        gpio_button: 13
    display:
        engine: oled

Run on PC with pygame display and keyboard control.

.. code-block:: yaml

    control:
        engine: keyboard
    display:
        engine: pygame

Start bluetooth config agent which allows you to configure wifi over bluetooth. This works only for RPI Zero.

.. code-block:: yaml

    bluetooth: true

https://github.com/Craftama/rpi3-wifi-conf-android

Home Assistant
==============

.. code-block:: yaml

    home_assistant:
        port: 8123
        password: pass
        host: localhost

Enable beta functions
=====================

.. code-block:: yaml

    beta: true

Development
===========

OS x

.. code-block:: bash

    brew install sdl sdl_image sdl_mixer sdl_ttf portmidi
    pip install luma.emulator

Pygame
======

Uses luma driver with pygame gui.

OLED Display
============

Uses luma driver for rendering.

LCD Display
===========

Currently supports only 20x4

http://www.dx.com/p/5v-iic-i2c-3-1-blue-screen-lcd-display-module-for-arduino-green-black-232993

Requires `python3-smbus`

Source of code https://github.com/bradgillap/I2C-LCD-Display
